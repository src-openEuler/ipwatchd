Name:    ipwatchd
Version: 1.3.0
Release: 5
Summary: IPwatchD is a simple daemon that analyses all incoming ARP packets in order to detect IP conflicts on Linux
License: GPLv2
URL:	 https://ipwatchd.sourceforge.io/
Source0: https://github.com/jariq/IPwatchD/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires: 	gcc autoconf automake libnet-devel libpcap-devel systemd chrpath

%description
IPwatchD can be configured to listen on one or more interfaces (alias interfaces are also supported) in active or passive mode. In active mode it protects your host before IP takeover by answering Gratuitous ARP requests received from conflicting system. In passive mode it just records information about conflict through standard syslog interface.

%prep
%setup -q -n IPwatchD-%{version}/

%build
%make_build -C src

%install
%make_install -C src DESTDIR=%{buildroot}
chrpath -d %{buildroot}%{_sbindir}/ipwatchd
install -d %{buildroot}/%{_unitdir}
mv %{buildroot}/lib/systemd/system/* %{buildroot}/%{_unitdir}

%pre
%preun
%systemd_preun ipwatchd.service
%post
%systemd_post ipwatchd.service
%postun
%systemd_postun_with_restart ipwatchd.service

%check

%files
%license LICENSE
%doc INSTALL README.md
%config(noreplace) %{_sysconfdir}/ipwatchd.conf
%{_unitdir}/ipwatchd.service
%{_sbindir}/*
%{_mandir}/*


%changelog
* Fri Mar 29 2024 Ge Wang <wang__ge@126.com> - 1.3.0-5
- fix source url

* Fri Sep 10 2021 Pengju Jiang <jiangpengju2@huawei.com> - 1.3.0-4
- solve the rpath problem

* Mon Jun 07 2021 wulei <wulei80@huawei.com> - 1.3.0-3
- fixes failed: File must begin with "/": %{_unitdir}/ipwatchd.server

* Tue Oct 13 2020 liqingqing_1229 <liqingqing3@huawei.com> - 1.3.0-2
- update source0

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com> - 1.3.0-1
- Package init

